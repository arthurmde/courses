# Creating a Blog with Rails

Based on [Rails Tutorial](http://guides.rubyonrails.org/getting_started.html)

## Create a new Rails project

* Install Ruby
* Install Rails
* Create a new rails project: `rails new blog`
* Explore generated folders
* Run the app
* Commit =)

## Say "Hello", Rails

* Generate a new controller: `rails generate controller Welcome index`
* Understand the list of created files
* See `app/controllers/welcome_controller.rb` file
* Add the "Hello, Rails!" message in view file
`app/views/welcome/index.html.erb`
* Run the app and access the /welcome/index page
* Commit =)

## Set homepage

* Open the file config/routes.rb
* Add `root 'welcome#index'`
* Remove the other route
* Run the app and access the homepage
* Commit =)

## Blog Requirements

* **Create** and **edit** articles
* **List** all blog articles
* **Read** specific article
* **Create** comments on articles

## Create Article MVC features

* Add `resources :articles` to config/routes.rb
* See routes: `rake routes`
* Run the app and access /articles/new
* Solve the problem generating a new controller: `rails g controller Articles`
* Create the **new** action
* Run the app and access /articles/new
* Solve the problem creating a new view: `app/views/articles/new.html.erb`

* Create Article model: `rails generate model Article title:string text:text`
* Explore created files
* Run the migration: `rake db:migrate`
* Enter in rails console to interact with ActiveRecord interface

* Create the 'create' action
* Add the following code to register an article on database:

```
def create
  @article = Article.new(params[:article])
   
  @article.save
  redirect_to @article
end
```

* Open the view file 'new.html.erb'
* Add an title and a new form:

```
<%= form_for :article, url: articles_path do |f| %>
  <p>
    <%= f.label :title %><br>
    <%= f.text_field :title %>
  </p>

  <p>
    <%= f.label :text %><br>
    <%= f.text_area :text %>
  </p>

  <p>
    <%= f.submit %>
  </p>
<% end %>
```

* Run the app and try to create a new article
* Fix your application by adding the following code to allow the register of
required fields:

```
def article_params
  params.require(:article).permit(:title, :text)
end
```

* Run the app and try to create a new article
* Verify in the console if the article was created
* Fix the code to render an article page


* Write the code to show the list of all articles (index):

```
<h1>Listing articles</h1>

  <table>
   <tr>
   <th>Title</th>
   <th>Text</th>
   </tr>

   <% @articles.each do |article| %>
   <tr>
   <td><%= article.title %></td>
   <td><%= article.text %></td>
   <td><%= link_to 'Show', article_path(article) %></td>
   </tr>
   <% end %>
   </table>
```

* Commit all changes =)

## Adding links on homepage

* Use **link_to** helper to add links on homepage

## Adding some validation

* Add the following code in Article model to validate **title** field:

```
validates :title, presence: true, length: { minimum: 5 }
```

* Try to create an invalid article
* Validates the correcteness of create action, redirecting to the **new**
action when the object is not valid
* Create a new article object inside **new** action
* Use the following code to show errors messages into **new** view:
```
<% if @article.errors.any? %>
  <div id="error_explanation">
    <h2>
      <%= pluralize(@article.errors.count, "error") %> 
       prohibited this article from being saved:
    </h2>
    <ul>
    <% @article.errors.full_messages.each do |msg| %>
      <li><%= msg %></li>
    <% end %>
    </ul>
  </div>
<% end %>
```

* Comment the possibilities related to Update and Destroy articles

## Add Comments

* Use scaffold to create Comment MVC features: 
`rails g scaffold Comment commenter:string body:text article:references`
* See the current routes
* Fix the new routes insider of articles resources
* See the routes again
* Add `has_many comments` to Article model
* Fix the controllers and views to add comments to same page as article

## What's next?


